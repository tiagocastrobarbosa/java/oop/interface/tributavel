public class CalculadoraDeImposto {

	private double totalImposto;
	
	public void registra(Tributavel t) {
		totalImposto += t.getValorImposto();
	}
	
	public double getTotalImposto() {
		return this.totalImposto;
	}
	
}
