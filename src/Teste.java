public class Teste {
	
	public static void main(String[] args) {
		
		ContaCorrente contaCorrente = new ContaCorrente();
		contaCorrente.salario = 3000.0;
		System.out.println(contaCorrente.getValorImposto());
		
		SeguroDeVida seguroDeVida = new SeguroDeVida();
		System.out.println(seguroDeVida.getValorImposto());
		
		CalculadoraDeImposto calculadoraDeImposto = new CalculadoraDeImposto();		
		calculadoraDeImposto.registra(contaCorrente);
		calculadoraDeImposto.registra(seguroDeVida);
		System.out.println(calculadoraDeImposto.getTotalImposto());
		
	}
	
}
