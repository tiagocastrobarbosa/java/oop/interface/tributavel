public class SeguroDeVida implements Tributavel {

	private double valor = 300;
	
	@Override
	public double getValorImposto() {
		return valor * 0.3;
	}	
	
}
